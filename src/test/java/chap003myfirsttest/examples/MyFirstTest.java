package chap003myfirsttest.examples;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class MyFirstTest {

    @Test
    public void canAddTwoPlusTwo() {

        int answer = 2 + 2;

        assertEquals("2+2=4",4, answer);

    }
    @Test
    public void twoMinusTwoEqualsZero() {
        int answer = 2 - 2;

        assertEquals("2-2=0", 0, answer);
    }
}
