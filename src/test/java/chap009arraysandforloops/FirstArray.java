package chap009arraysandforloops;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FirstArray {

    @Test
    public void simpleArrayExample() {
        String[] numbers0123 = {"zero", "one", "two", "three"};

        for (String numberText : numbers0123) {
            System.out.println(numberText);
        }

        assertEquals("zero", numbers0123[0]);
        assertEquals("three", numbers0123[3]);

    }
}
