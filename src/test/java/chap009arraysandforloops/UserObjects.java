package chap009arraysandforloops;

import org.junit.Test;

public class UserObjects {
    @Test
    public void simpleArrayExample() {
        String[] users = {"Curley", "Larry", "Mo"};

        for (String usernames : users) {
            System.out.println(usernames);
        }
    }
}
