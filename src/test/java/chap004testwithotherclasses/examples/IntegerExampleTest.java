package chap004testwithotherclasses.examples;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IntegerExampleTest {

    @Test
    public void integerExploration() {

        Integer four = new Integer(4);
        assertEquals("intValue returns 4", 4, four.intValue());

        Integer five = new Integer("5");
        assertEquals("intValue returns 5", 5, five.intValue());

        Integer six = 6;
        assertEquals(" autoboxing assignment for 6", 6, six.intValue());

    }
    @Test
    public void hexTests(){
        
        Integer.toHexString(3);
        assertEquals("3", Integer.toHexString(3));
    }
}
